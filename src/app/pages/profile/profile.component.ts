import {Component, OnDestroy} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators' ;
import { UserService }  from '../../services/user/user.service';


@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./profile.component.scss'],
  templateUrl: './profile.component.html',
})
export class ProfileComponent implements OnDestroy {
  user;

  constructor(private themeService: NbThemeService,private userService: UserService) {
  }

  ngOnInit() {
    this.user=this.userService.getLoggedInUser();
  }

  ngOnDestroy() {
  }
}
