import { NgModule } from '@angular/core';
import {
  NbButtonModule,
  NbCardModule,
} from '@nebular/theme';
import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { FormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

@NgModule({
  imports: [
    FormsModule,
    ThemeModule,
    NbCardModule,
    NbButtonModule,
    Ng2SmartTableModule,
    LeafletModule,
  ],
  declarations: [
    DashboardComponent,
  ],
})
export class DashboardModule { }
