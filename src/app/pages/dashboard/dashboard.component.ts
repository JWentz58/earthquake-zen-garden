import {Component, OnDestroy} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators' ;
import { LocalDataSource } from 'ng2-smart-table';
import { EarthquakesService }  from '../../services/earthquakes/earthquakes.service';

import { latLng, LatLng, tileLayer, circle } from 'leaflet';
import 'style-loader!leaflet/dist/leaflet.css';

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnDestroy {

  private alive = true;
  settings = {
    actions:{
      add: false,
      edit: false,
      delete:false,
      custom:[{name:'custom', title:'<i class="fa fa-eye"></i>'}]
    },
    columns: {
      type: {
        title: 'Type',
        type: 'string',
      },
      url: {
        title: 'Details',
        type: 'string',
      },
      place: {
        title: 'Place',
        type: 'string',
      },
      mag: {
        title: 'Mag',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();
  data:any;
  tableData:any;

  options = {
  	layers: [
  		tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })

  	],
  	zoom: 0,
  	center: latLng(46.879966, -121.726909)
  };

  earthquakePointMarker = [
  ];

  selectedEarthQuake;

  constructor(private service: EarthquakesService) {
    this.data=this.service.getData();
    this.tableData=[];
    for(var f in this.data.features){
      var fea = this.data.features[f].properties;
      fea.id = this.data.features[f].id
      this.tableData.push(fea)
    }

    this.source.load(this.tableData);
  }

  viewClicked(event){
    this.selectedEarthQuake= null;
    this.options.center = null;
    this.earthquakePointMarker = null;

    for(var f in this.data.features){
      if(this.data.features[f].id == event.data.id)
        this.selectedEarthQuake = this.data.features[f]
    }
    this.options.center = latLng(this.selectedEarthQuake.geometry.coordinates[1], this.selectedEarthQuake.geometry.coordinates[0])
    this.earthquakePointMarker = [
      circle([ this.selectedEarthQuake.geometry.coordinates[1], this.selectedEarthQuake.geometry.coordinates[0] ], { radius: 5000 })
    ]

  }

  ngOnDestroy() {
    this.alive = false;
  }
}
