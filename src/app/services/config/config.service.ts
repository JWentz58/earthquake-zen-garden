import { Injectable } from '@angular/core';

@Injectable(
  {providedIn: 'root'}
)
export class ConfigService {

 siteInfo = {
     "title": "Earthquake Zen Garden",
     "heroImage": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/74/Arenal_Volcano_07_2015_CRI_3828.jpg/1920px-Arenal_Volcano_07_2015_CRI_3828.jpg",
     "logoImage": "https://upload.wikimedia.org/wikipedia/commons/c/ca/Realtor-com_logo.png",
     "logoImageAsset":"assets/images/realtor-com.png"
    }
  //Typically this service would be init from some Auth object ie JWT
  constructor() {
  }

  public getSiteInfo(){
    return this.siteInfo;
  }


}
