import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable(
  {providedIn: 'root'}
)
export class FunkyJsonService {
  public data: any[];
  public assetURL = 'assets/data/'
  constructor(private http: HttpClient) {

  }

  public async getFullJSON(){
    return this.http.get(this.assetURL+"crazy_json_data.json")
  }

  public async getUserJson(){
    return this.http.get(this.assetURL+"user_profile.json")
  }

  public async getSiteJson(){
    return this.http.get(this.assetURL+"site.json")
  }

}
