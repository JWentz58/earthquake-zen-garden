# Earthquake Zen Garden
## Build with Angular and Nebular/NGX-Admin Framework

### Setup
git clone repo
yarn install
yarn start
access: http://localhost:4200


### Known Issues
- SCSS could use some work on the labels/forms
- Its not in React
- leaflet map zoom level is out to far and having issues with recentering map on point change. 
- Earthquake details not routed to a new Page -- the details is displayed below the table
- Table does not scroll to the details on 'eye' click
- Route in sidebar not going to Home must click the icon
- some junk in the package.json could be removed along with some of the testing files that are not used.
